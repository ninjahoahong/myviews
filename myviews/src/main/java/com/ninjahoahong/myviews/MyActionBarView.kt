package com.ninjahoahong.myviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.view_my_action_bar.view.*


class MyActionBarView : LinearLayout {

    private var isLeftButtonActive = false
    private var isRightButtonActive = false

    constructor(context: Context) : super(context) {
        initView(context, null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView(context, attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView(context, attrs, defStyleAttr)
    }

    private fun initView(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
        LayoutInflater.from(context).inflate(R.layout.view_my_action_bar, this, true)
        orientation = HORIZONTAL
        if (isLeftButtonActive) {
            leftButton.visibility = View.VISIBLE
        } else {
            leftButton.visibility = View.GONE
        }
        if (isRightButtonActive) {
            rightButton.visibility = View.VISIBLE
        } else {
            rightButton.visibility = View.GONE
        }
    }

    fun setActionBarBackgroudColor(color: Int) {
        this.setBackgroundColor(color)
    }

    fun setLeftButton(isActive: Boolean, icon: Int, action: OnClickListener) {
        this.isLeftButtonActive = isActive
        if (isLeftButtonActive) {
            leftButton.visibility = View.VISIBLE
            leftButton.setImageResource(icon)
            leftButton.setOnClickListener(action)
        } else {
            leftButton.visibility = View.GONE
        }
    }

    fun setRightButton(isActive: Boolean, icon: Int, action: OnClickListener) {
        this.isRightButtonActive = isActive
        if (isRightButtonActive) {
            rightButton.visibility = View.VISIBLE
            rightButton.setImageResource(icon)
            rightButton.setOnClickListener(action)
        } else {
            rightButton.visibility = View.GONE
        }
    }

    fun setActionBarTitle(title: String, color: Int) {
        actionBarTitle.text = title
        actionBarTitle.setTextColor(color)
    }
}
